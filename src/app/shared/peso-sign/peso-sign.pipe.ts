import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pesoSign',
})
export class PesoSignPipe implements PipeTransform {
  transform(value: any, ...args: any[]) {
    return `₱${value}`;
  }
}

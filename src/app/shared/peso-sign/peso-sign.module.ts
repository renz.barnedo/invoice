import { NgModule } from '@angular/core';
import { PesoSignPipe } from './peso-sign.pipe';

@NgModule({
  declarations: [PesoSignPipe],
  exports: [PesoSignPipe],
})
export class PesoSignModule {}

export type InvoiceStatus = 'paid' | 'pending' | 'draft';

export interface Address {
  street: string;
  city: string;
  postCode: string;
  country: string;
}

export type PaymentTermOptionLabel =
  | 'Net 1 Day'
  | 'Net 7 Days'
  | 'Net 14 Days'
  | 'Net 30 Days';
export type PaymentTerm = 1 | 7 | 14 | 30;

export interface PaymentTermOption {
  label: PaymentTermOptionLabel;
  value: PaymentTerm;
}
export type PaymentTermOptions = PaymentTermOption[];
export const paymentTermOptions: PaymentTermOptions = [
  {
    label: 'Net 1 Day',
    value: 1,
  },
  {
    label: 'Net 7 Days',
    value: 7,
  },
  {
    label: 'Net 14 Days',
    value: 14,
  },
  {
    label: 'Net 30 Days',
    value: 30,
  },
];

export interface Item {
  name: string;
  quantity: number;
  price: number;
  total: number;
}

export type Items = Item[];

export interface Invoice {
  id: string;
  createdAt: Date;
  paymentDue: Date;
  description: string;
  paymentTerms: number;
  clientName: string;
  clientEmail: string;
  status: InvoiceStatus;
  senderAddress: Address;
  clientAddress: Address;
  items: Items;
  total: number;
}

export type Invoices = Invoice[];

export const defaultInvoice: Invoice = {
  id: '',
  createdAt: new Date(),
  paymentDue: new Date(),
  description: '',
  paymentTerms: 0,
  clientName: '',
  clientEmail: '',
  status: 'pending',
  senderAddress: {
    street: '',
    city: '',
    postCode: '',
    country: '',
  },
  clientAddress: {
    street: '',
    city: '',
    postCode: '',
    country: '',
  },
  items: [
    {
      name: '',
      quantity: 1,
      price: 1,
      total: 1,
    },
  ],
  total: 0,
};

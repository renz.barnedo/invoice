export const LABEL_PAID = 'paid';
export const LABEL_PENDING = 'pending';
export const LABEL_DRAFT = 'draft';
export type DropdownLabel = 'paid' | 'pending' | 'draft';

export type DropdownOption = {
  label: DropdownLabel;
  isChecked: boolean;
};

export type DropdownOptions = DropdownOption[];

export const options: DropdownOptions = [
  {
    label: 'paid',
    isChecked: true,
  },
  {
    label: 'pending',
    isChecked: true,
  },
  {
    label: 'draft',
    isChecked: true,
  },
];

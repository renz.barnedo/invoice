import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { exhaustMap, filter, map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import {
  DropdownOption,
  DropdownOptions,
  options,
} from './constants/dropdown-option.constants';
import {
  defaultInvoice,
  Invoice,
  Invoices,
} from './constants/invoice.constants';
import { DARK_THEME, LIGHT_THEME, Theme } from './constants/theme.constants';

interface ApiResponse {
  data: Invoices;
}

@Injectable({
  providedIn: 'root',
})
export class SharedService {
  private invoicesSource = new BehaviorSubject<Invoices>([]);
  currentInvoices = this.invoicesSource.asObservable();

  private options = options;
  private optionsSource = new BehaviorSubject<DropdownOptions>(this.options);
  currentOptions = this.optionsSource.asObservable();

  private pickedInvoiceSource = new BehaviorSubject<Invoice>(defaultInvoice);
  currentPickedInvoice = this.pickedInvoiceSource.asObservable();

  private deletePopUpSource = new BehaviorSubject<string>('');
  currentDeletePopUp = this.deletePopUpSource.asObservable();

  private invoiceForEditSource = new BehaviorSubject<Invoice>(defaultInvoice);
  currentInvoiceForEdit = this.invoiceForEditSource.asObservable();

  private formIsOpenSource = new BehaviorSubject<boolean>(false);
  currentFormIsOpen = this.formIsOpenSource.asObservable();

  private themeSource = new BehaviorSubject<Theme>(DARK_THEME);
  currentTheme = this.themeSource.asObservable();

  private profilepopUpSource = new BehaviorSubject<boolean>(false);
  currentProfilePopUp = this.profilepopUpSource.asObservable();

  API_URL = environment.apiUrl;

  constructor(private http: HttpClient) {}

  toggleProfilePopup(isOpen: boolean) {
    this.profilepopUpSource.next(isOpen);
  }

  toggleTheme(theme: Theme) {
    const changedTo = theme === DARK_THEME ? LIGHT_THEME : DARK_THEME;
    this.themeSource.next(changedTo);
  }

  toggleForm(opened: boolean) {
    this.formIsOpenSource.next(opened);
  }

  getInvoices() {
    return this.http.get<ApiResponse>(this.API_URL).pipe(
      map((response: ApiResponse) => this.filterInvoices(response.data)),
      exhaustMap((invoices: Invoices) => {
        this.invoicesSource.next(invoices);
        return this.currentInvoices;
      })
    );
  }

  async getInvoicesForUpdate(): Promise<Invoices> {
    return await new Promise((resolve, reject) =>
      this.getInvoices().subscribe(
        (invoices: Invoices) => resolve(invoices || []),
        (error: Error) => {
          alert('API request limit. Please try again');
          return reject(error);
        }
      )
    );
  }

  getInvoice(invoice: Invoice) {
    this.pickedInvoiceSource.next(invoice);
  }

  async getInvoiceById(id: string) {
    const invoices = await this.getInvoicesForUpdate();
    const picked = invoices.find((invoice: Invoice) => invoice.id === id);
    this.pickedInvoiceSource.next(picked || defaultInvoice);
  }

  addId(invoice: Invoice) {
    return { ...invoice, id: 'RT3082' };
  }

  setInvoiceForEdit(invoice: Invoice) {
    this.invoiceForEditSource.next(invoice);
  }

  updateApiData(invoices: Invoices) {
    return this.http.post<Invoice>(
      this.API_URL,
      { data: invoices },
      {
        responseType: 'text' as 'json',
      }
    );
  }

  async addInvoice(invoice: Invoice) {
    const fullInvoice = this.addId(invoice);
    const invoices = await this.getInvoicesForUpdate();
    invoices.push(fullInvoice);
    return this.updateApiData(invoices).pipe(
      map(() => {
        this.invoicesSource.next(invoices);
        return fullInvoice;
      })
    );
  }

  async updateInvoice(forUpdate: Invoice) {
    let invoices = await this.getInvoicesForUpdate();
    invoices = invoices.map((invoice: Invoice) =>
      invoice.id === forUpdate.id ? forUpdate : invoice
    );
    this.invoicesSource.next(invoices);
    return this.updateApiData(invoices).pipe(map(() => forUpdate));
  }

  async deleteInvoice(id: string) {
    let invoices = await this.getInvoicesForUpdate();
    invoices = invoices.filter((invoice: Invoice) => invoice.id !== id);
    this.invoicesSource.next(invoices);
    return this.updateApiData(invoices);
  }

  getHeaderLabel(total: number) {
    let beVerb = total > 1 ? 'are' : 'is';
    let plurality = total > 1 ? ' invoices' : ' invoice';
    let predicate = !this.checkedOptions.length
      ? 'no filter selected'
      : this.checkedOptions.length === this.options.length
      ? 'total' + plurality
      : this.checkedOptionsLabel + plurality;
    return `There ${beVerb} ${total} ${predicate}.`;
  }

  get checkedOptionsLabel() {
    return this.checkedOptions
      .map((option: DropdownOption) => option.label)
      .join(' & ');
  }

  get checkedOptions() {
    return this.options.filter((option: DropdownOption) => option.isChecked);
  }

  filterInvoices(invoices: Invoices) {
    const checked = this.checkedOptions;
    let filtered: Invoices = [];
    if (checked.length === this.options.length) {
      filtered = invoices;
    } else if (checked.length) {
      filtered = invoices.filter((invoice: Invoice) =>
        checked.find(
          (option: DropdownOption) => option.label === invoice.status
        )
      );
    }
    this.invoicesSource.next(filtered);
    return filtered;
  }

  async checkOption(index: number) {
    const hoveredOption = this.options[index];
    hoveredOption.isChecked = !hoveredOption.isChecked;
    this.optionsSource.next(this.options);
    const invoices = await this.getInvoicesForUpdate();

    this.filterInvoices(invoices);
  }

  toggleDeletePopup(id: string) {
    this.deletePopUpSource.next(id);
  }
}

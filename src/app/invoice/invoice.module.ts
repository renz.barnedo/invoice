import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvoiceRoutingModule } from './invoice-routing.module';
import { InvoiceComponent } from './invoice.component';
import { StatusBoxModule } from '../widgets/status-box/status-box.module';
import { IconChevronModule } from '../widgets/icon-chevron/icon-chevron.module';
import { HeaderComponent } from './header/header.component';
import { CardComponent } from './card/card.component';
import { PesoSignModule } from '../shared/peso-sign/peso-sign.module';
import { IdModule } from '../widgets/id/id.module';
import { DeletePopupModule } from '../widgets/delete-popup/delete-popup.module';

@NgModule({
  declarations: [InvoiceComponent, HeaderComponent, CardComponent],
  imports: [
    CommonModule,
    InvoiceRoutingModule,
    StatusBoxModule,
    IconChevronModule,
    PesoSignModule,
    IdModule,
    DeletePopupModule,
  ],
})
export class InvoiceModule {}

import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Invoice } from '../shared/constants/invoice.constants';
import { SharedService } from '../shared/shared.service';
import { map, exhaustMap } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { LABEL_PAID } from '../shared/constants/dropdown-option.constants';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss'],
})
export class InvoiceComponent implements OnInit, OnDestroy {
  invoice!: Invoice;
  isExiting = false;
  invoiceSubscription!: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private sharedService: SharedService
  ) {}

  ngOnInit(): void {
    this.invoiceSubscription = this.route.params
      .pipe(
        map((params: Params) => params.id),
        exhaustMap((id: string) => {
          this.sharedService.getInvoiceById(id);
          return this.sharedService.currentPickedInvoice;
        })
      )
      .subscribe((invoice: Invoice) => {
        this.invoice = invoice;
      });
  }

  ngOnDestroy(): void {
    this.invoiceSubscription.unsubscribe();
  }

  async onBackButtonClick() {
    this.isExiting = true;
    await new Promise((resolve) => setTimeout(resolve, 600));
    this.router.navigate(['/']);
  }

  openDeletePopUp() {
    this.sharedService.toggleDeletePopup(this.invoice.id);
  }

  async markInvoiceAsPaid() {
    const invoice: Invoice = { ...this.invoice, status: LABEL_PAID };
    this.sharedService.getInvoice(invoice);
    (await this.sharedService.updateInvoice(invoice)).subscribe();
  }

  async editInvoice() {
    this.sharedService.setInvoiceForEdit(this.invoice);
    this.sharedService.toggleForm(true);
  }

  async deleteInvoice() {
    (await this.sharedService.deleteInvoice(this.invoice.id)).subscribe();
    await this.onBackButtonClick();
  }
}

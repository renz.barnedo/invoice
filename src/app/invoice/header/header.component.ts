import { Component, EventEmitter, Input, Output } from '@angular/core';
import {
  DropdownLabel,
  LABEL_PENDING,
} from 'src/app/shared/constants/dropdown-option.constants';

@Component({
  selector: 'app-header',
  template: `
    <header class="header">
      <span class="header__status-label">Status</span>
      <app-status-box [status]="status"></app-status-box>
      <button
        class="btn btn--primary"
        *ngIf="status === LABEL_PENDING"
        (click)="onMarkAsPaidButtonClick()"
      >
        Mark As Paid
      </button>
      <button class="btn btn--secondary" (click)="onEditButtonClick()">
        Edit
      </button>
      <button class="btn btn--danger" (click)="onDeleteButtonClick()">
        Delete
      </button>
    </header>
  `,
  styles: [
    `
      @use '../../../styles/colors';
      @use '../../../styles/defaults';
      .header {
        display: grid;
        align-items: center;
        grid-template-columns: auto 1fr auto auto auto;
        gap: 1.6rem;
        padding: 2.8rem;
        border-radius: defaults.$border-radius;
        background-color: colors.$primary-accent;
        margin-bottom: 2.4rem;
      }
      @media (max-width: 650px) {
        .header {
          grid-template-columns: 1fr 1fr auto;

          &__status-label {
            grid-column: 1 / span 2;
          }
        }
      }
    `,
  ],
})
export class HeaderComponent {
  @Input() status: DropdownLabel = 'draft';
  @Output() markAsPaidButtonClick = new EventEmitter<void>();
  @Output() editButtonClick = new EventEmitter<void>();
  @Output() deleteButtonClick = new EventEmitter<void>();
  LABEL_PENDING: DropdownLabel = LABEL_PENDING;

  onMarkAsPaidButtonClick() {
    this.markAsPaidButtonClick.emit();
  }

  onEditButtonClick() {
    this.editButtonClick.emit();
  }

  onDeleteButtonClick() {
    this.deleteButtonClick.emit();
  }
}

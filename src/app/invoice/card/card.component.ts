import { Component, Input } from '@angular/core';
import {
  defaultInvoice,
  Invoice,
} from 'src/app/shared/constants/invoice.constants';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent {
  @Input() invoice = defaultInvoice;
}

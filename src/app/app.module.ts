import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { FormModule } from './form/form.module';
import { SidebarModule } from './widgets/sidebar/sidebar.module';

import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormModule,
    SidebarModule,
    HttpClientModule,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

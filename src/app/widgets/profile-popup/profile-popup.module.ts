import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfilePopupComponent } from './profile-popup.component';

@NgModule({
  declarations: [ProfilePopupComponent],
  imports: [CommonModule],
  exports: [ProfilePopupComponent],
})
export class ProfilePopupModule {}

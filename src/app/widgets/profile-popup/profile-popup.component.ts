import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { SharedService } from 'src/app/shared/shared.service';

@Component({
  selector: 'app-profile-popup',
  templateUrl: './profile-popup.component.html',
  styleUrls: ['./profile-popup.component.scss'],
})
export class ProfilePopupComponent implements OnInit, OnDestroy {
  show = false;
  developer = {
    label: 'renzbarnedo.me',
    websiteUrl: 'https://renzbarnedo.me',
  };
  popupSubscription!: Subscription;
  @Output() overlayClick = new EventEmitter<boolean>();

  constructor(private sharedService: SharedService) {}

  ngOnInit(): void {
    this.popupSubscription = this.sharedService.currentProfilePopUp.subscribe(
      (isOpen: boolean) => (this.show = isOpen)
    );
  }

  ngOnDestroy(): void {
    this.popupSubscription.unsubscribe();
  }

  exitPopup() {
    this.overlayClick.emit(false);
  }
}

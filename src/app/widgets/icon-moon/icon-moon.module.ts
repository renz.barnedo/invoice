import { NgModule } from '@angular/core';
import { IconMoonComponent } from './icon-moon.component';

@NgModule({
  declarations: [IconMoonComponent],
  exports: [IconMoonComponent],
})
export class IconMoonModule {}

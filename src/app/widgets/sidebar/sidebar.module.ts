import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar.component';
import { IconLogoModule } from '../icon-logo/icon-logo.module';
import { IconProfileModule } from '../icon-profile/icon-profile.module';
import { IconThemeModule } from '../icon-theme/icon-theme.module';
import { ProfilePopupModule } from '../profile-popup/profile-popup.module';

@NgModule({
  declarations: [SidebarComponent],
  imports: [
    CommonModule,
    IconLogoModule,
    IconThemeModule,
    IconProfileModule,
    ProfilePopupModule,
  ],
  exports: [SidebarComponent],
})
export class SidebarModule {}

import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Theme } from 'src/app/shared/constants/theme.constants';
import { SharedService } from 'src/app/shared/shared.service';

@Component({
  selector: 'app-sidebar',
  template: `
    <aside class="nav">
      <app-icon-logo class="nav__icon-logo"></app-icon-logo>
      <app-icon-profile
        (iconClick)="toggleProfilePopup($event)"
        class="nav__icon-profile"
      ></app-icon-profile>
      <app-profile-popup
        (overlayClick)="toggleProfilePopup($event)"
      ></app-profile-popup>
    </aside>
  `,
  styles: [
    `
      @use '../../../styles/colors';
      @use '../../../styles/defaults';

      .nav {
        position: fixed;
        left: 0;
        top: 0;
        bottom: 0;
        width: defaults.$sidebar-width;
        height: 100vh;
        border-top-right-radius: defaults.$large-border-radius;
        border-bottom-right-radius: defaults.$large-border-radius;
        background-color: colors.$primary-accent;
        z-index: 3;
        display: grid;
        grid-template-rows: 1fr auto auto;

        &__icon-logo {
          width: defaults.$sidebar-width;
          height: defaults.$sidebar-width;
        }

        &__icon-profile {
          border-top: 0.1rem solid colors.$draft-transparent;
          height: 10vh;
          width: 100%;
        }
      }

      @media (max-width: 900px) {
        .nav {
          grid-template-rows: auto;
          grid-template-columns: 1fr auto;
          width: 100vw;
          height: defaults.$sidebar-width-tablet;

          &__icon-logo {
            width: defaults.$sidebar-width-tablet;
            height: defaults.$sidebar-width-tablet;
          }

          &__icon-profile {
            width: 10rem;
            height: defaults.$sidebar-width-tablet;
            border-top: none;
            border-left: 0.1rem solid colors.$draft-transparent;
          }
        }
      }
    `,
  ],
})
export class SidebarComponent implements OnInit, OnDestroy {
  currentTheme!: Theme;
  themeSubscription!: Subscription;

  constructor(private sharedService: SharedService) {}

  ngOnInit(): void {
    this.themeSubscription = this.sharedService.currentTheme.subscribe(
      (currentTheme: Theme) => (this.currentTheme = currentTheme)
    );
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }

  toggleTheme() {
    this.sharedService.toggleTheme(this.currentTheme);
  }

  toggleProfilePopup(open: boolean) {
    this.sharedService.toggleProfilePopup(open);
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatusBoxComponent } from './status-box.component';

@NgModule({
  declarations: [StatusBoxComponent],
  imports: [CommonModule],
  exports: [StatusBoxComponent],
})
export class StatusBoxModule {}

import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { SharedService } from 'src/app/shared/shared.service';

@Component({
  selector: 'app-delete-popup',
  templateUrl: './delete-popup.component.html',
  styleUrls: ['./delete-popup.component.scss'],
})
export class DeletePopupComponent implements OnInit, OnDestroy {
  id = '';
  popupSubscription!: Subscription;
  @Output() deleteButtonClick = new EventEmitter<void>();

  constructor(private sharedService: SharedService, private router: Router) {}

  ngOnInit(): void {
    this.popupSubscription = this.sharedService.currentDeletePopUp.subscribe(
      (id: string) => (this.id = id)
    );
  }

  ngOnDestroy(): void {
    this.popupSubscription.unsubscribe();
  }

  onCancelButtonClick() {
    this.sharedService.toggleDeletePopup('');
  }

  onDeleteButtonClick() {
    this.sharedService.toggleDeletePopup('');
    this.deleteButtonClick.emit();
  }
}

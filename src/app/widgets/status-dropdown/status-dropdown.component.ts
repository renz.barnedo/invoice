import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { DropdownOptions } from 'src/app/shared/constants/dropdown-option.constants';
import { SharedService } from 'src/app/shared/shared.service';

@Component({
  selector: 'app-status-dropdown',
  templateUrl: './status-dropdown.component.html',
  styleUrls: ['./status-dropdown.component.scss'],
})
export class StatusDropdownComponent implements OnInit, OnDestroy {
  options!: DropdownOptions;
  show = false;
  getOptionsSubscription!: Subscription;

  constructor(private sharedService: SharedService) {}

  ngOnInit(): void {
    this.getOptionsSubscription = this.sharedService.currentOptions.subscribe(
      (options: DropdownOptions) => (this.options = options)
    );
  }

  ngOnDestroy(): void {
    this.getOptionsSubscription.unsubscribe();
  }

  onOptionClick(index: number) {
    // TODO: make this a directive maybe
    this.sharedService.checkOption(index);
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatusDropdownComponent } from './status-dropdown.component';
import { IconChevronModule } from '../icon-chevron/icon-chevron.module';

@NgModule({
  declarations: [StatusDropdownComponent],
  imports: [CommonModule, IconChevronModule],
  exports: [StatusDropdownComponent],
})
export class StatusDropdownModule {}

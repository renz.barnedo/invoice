import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconProfileComponent } from './icon-profile.component';

@NgModule({
  declarations: [IconProfileComponent],
  imports: [CommonModule],
  exports: [IconProfileComponent],
})
export class IconProfileModule {}

import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-icon-profile',
  template: `
    <div class="container">
      <img
        class="container__logo"
        (click)="onIconClick()"
        src="../../../assets/img/me.jpg"
        alt="a picture of Renz Barnedo"
      />
    </div>
  `,
  styles: [
    `
      .container {
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100%;

        &__logo {
          width: 3.2rem;
          height: 3.2rem;
          background-color: white;
          border-radius: 50%;
          cursor: pointer;
        }
      }
    `,
  ],
})
export class IconProfileComponent {
  @Output() iconClick = new EventEmitter<boolean>();

  onIconClick() {
    this.iconClick.emit(true);
  }
}

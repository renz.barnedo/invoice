import { NgModule } from '@angular/core';
import { IconSunComponent } from './icon-sun.component';

@NgModule({
  declarations: [IconSunComponent],
  exports: [IconSunComponent],
})
export class IconSunModule {}

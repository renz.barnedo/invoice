import { Component } from '@angular/core';

@Component({
  selector: 'app-icon-chevron',
  template: `
    <svg
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      class="chevron"
      viewBox="0 0 24 24"
      stroke="currentColor"
    >
      <path
        stroke-linecap="round"
        stroke-linejoin="round"
        stroke-width="2"
        d="M19 9l-7 7-7-7"
      />
    </svg>
  `,
  styles: [
    `
      @use '../../../styles/colors';
      .chevron {
        width: 1.4rem;
        height: 1.4rem;
        stroke: colors.$primary;
      }
    `,
  ],
})
export class IconChevronComponent {}

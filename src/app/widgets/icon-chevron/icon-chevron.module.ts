import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconChevronComponent } from './icon-chevron.component';

@NgModule({
  declarations: [IconChevronComponent],
  imports: [CommonModule],
  exports: [IconChevronComponent],
})
export class IconChevronModule {}

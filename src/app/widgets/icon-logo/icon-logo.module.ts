import { NgModule } from '@angular/core';
import { IconLogoComponent } from './icon-logo.component';

@NgModule({
  declarations: [IconLogoComponent],
  exports: [IconLogoComponent],
})
export class IconLogoModule {}

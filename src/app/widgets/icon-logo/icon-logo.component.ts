import { Component } from '@angular/core';

@Component({
  selector: 'app-icon-logo',
  template: `
    <div class="container">
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="28"
        height="26"
        class="container__icon"
      >
        <path
          fill="#FFF"
          fill-rule="evenodd"
          d="M20.513 0C24.965 2.309 28 6.91 28 12.21 28 19.826 21.732 26 14 26S0 19.826 0 12.21C0 6.91 3.035 2.309 7.487 0L14 12.9z"
        />
      </svg>
    </div>
  `,
  styles: [
    `
      @use '../../../styles/colors';
      @use '../../../styles/defaults';
      .container {
        width: 100%;
        height: 100%;
        background-color: colors.$primary;
        border-top-right-radius: defaults.$large-border-radius;
        border-bottom-right-radius: defaults.$large-border-radius;
        display: flex;
        justify-content: center;
        align-items: center;
        position: relative;

        &::after {
          content: '';
          position: absolute;
          top: 50%;
          right: 0;
          left: 0;
          width: 100%;
          height: 50%;
          border-top-left-radius: defaults.$large-border-radius;
          border-bottom-right-radius: defaults.$large-border-radius;
          background-color: colors.$logo-icon-transparent;
          z-index: 1;
          transition: all 0.3s;
        }

        &:hover {
          &::after {
            height: 100%;
            top: 0;
            border-top-left-radius: 0;
            border-top-right-radius: defaults.$large-border-radius;
          }
        }

        &__icon {
          transform: scale(1.5);
        }
      }

      @media (max-width: 900px) {
        .container {
          &__icon {
            transform: scale(1.25);
          }
        }
      }
    `,
  ],
})
export class IconLogoComponent {}

import { NgModule } from '@angular/core';
import { IdComponent } from './id.component';

@NgModule({
  declarations: [IdComponent],
  exports: [IdComponent],
})
export class IdModule {}

import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-id',
  template: ` <span class="tag">#</span><span class="id">{{ id }}</span> `,
  styles: [
    `
      @use '../../../styles/colors';
      .tag {
        color: colors.$secondary;
      }
    `,
  ],
})
export class IdComponent {
  @Input() id = '';
}

import { Component, EventEmitter, Input, Output } from '@angular/core';
import {
  DARK_THEME,
  LIGHT_THEME,
  Theme,
} from 'src/app/shared/constants/theme.constants';

@Component({
  selector: 'app-icon-theme',
  template: `
    <button class="btn icon-theme">
      <app-icon-sun
        *ngIf="currentTheme === DARK_THEME; else lightTheme"
        (click)="toggleTheme()"
      ></app-icon-sun>
      <ng-template #lightTheme>
        <app-icon-moon (click)="toggleTheme()"></app-icon-moon>
      </ng-template>
    </button>
  `,
  styles: [
    `
      .icon-theme {
        background-color: transparent;
        padding: 0;
      }
    `,
  ],
})
export class IconThemeComponent {
  DARK_THEME: Theme = DARK_THEME;
  LIGHT_THEME: Theme = LIGHT_THEME;
  @Input() currentTheme: Theme = DARK_THEME;
  @Output() themeChanged = new EventEmitter<void>();

  toggleTheme() {
    this.themeChanged.emit();
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconThemeComponent } from './icon-theme.component';
import { IconMoonModule } from '../icon-moon/icon-moon.module';
import { IconSunModule } from '../icon-sun/icon-sun.module';

@NgModule({
  declarations: [IconThemeComponent],
  imports: [CommonModule, IconMoonModule, IconSunModule],
  exports: [IconThemeComponent],
})
export class IconThemeModule {}

import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { SharedService } from 'src/app/shared/shared.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  @Input() label = '';
  formIsOpenSubscription!: Subscription;
  formIsOpen = false;

  constructor(private sharedService: SharedService) {}

  ngOnInit(): void {
    this.formIsOpenSubscription =
      this.sharedService.currentFormIsOpen.subscribe((value: boolean) => {
        this.formIsOpen = value;
      });
  }

  ngOnDestroy(): void {
    this.formIsOpenSubscription.unsubscribe();
  }

  openForm() {
    this.sharedService.toggleForm(!this.formIsOpen);
  }
}

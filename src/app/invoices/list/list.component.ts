import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Invoice, Invoices } from 'src/app/shared/constants/invoice.constants';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  @Input() invoices: Invoices = [];
  @Output() invoiceClicked = new EventEmitter<string>();

  constructor() {}

  ngOnInit(): void {}

  async onInvoiceClick(invoice: Invoice) {
    this.invoiceClicked.emit(invoice.id);
  }
}

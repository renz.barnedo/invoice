import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Invoices } from '../shared/constants/invoice.constants';
import { SharedService } from '../shared/shared.service';

@Component({
  selector: 'app-component',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.scss'],
})
export class InvoicesComponent implements OnInit, OnDestroy {
  invoices!: Invoices;
  getInvoiceSubscription!: Subscription;
  headerLabel!: string;
  fadeOutLeft = false;

  constructor(private sharedService: SharedService, private router: Router) {}

  ngOnInit(): void {
    this.getInvoiceSubscription = this.sharedService
      .getInvoices()
      .subscribe((invoices: Invoices) => {
        this.invoices = invoices;
        this.headerLabel = this.sharedService.getHeaderLabel(
          invoices?.length || 0
        );
      });
  }

  ngOnDestroy() {
    this.getInvoiceSubscription.unsubscribe();
  }

  async onInvoiceClick(invoiceId: string) {
    this.fadeOutLeft = true;
    await new Promise((resolve) => setTimeout(resolve, 600));
    this.router.navigate(['invoice', invoiceId]);
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PesoSignModule } from '../shared/peso-sign/peso-sign.module';
import { InvoicesRoutingModule } from './invoices-routing.module';
import { InvoicesComponent } from './invoices.component';
import { HeaderComponent } from './header/header.component';
import { ListComponent } from './list/list.component';
import { StatusDropdownModule } from '../widgets/status-dropdown/status-dropdown.module';
import { StatusBoxModule } from '../widgets/status-box/status-box.module';
import { IdModule } from '../widgets/id/id.module';

@NgModule({
  declarations: [InvoicesComponent, HeaderComponent, ListComponent],
  imports: [
    CommonModule,
    InvoicesRoutingModule,
    PesoSignModule,
    StatusDropdownModule,
    StatusBoxModule,
    IdModule,
  ],
})
export class InvoicesModule {}

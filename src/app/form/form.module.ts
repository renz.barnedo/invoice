import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormComponent } from './form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { PesoSignModule } from '../shared/peso-sign/peso-sign.module';

@NgModule({
  declarations: [FormComponent],
  imports: [CommonModule, ReactiveFormsModule, PesoSignModule],
  exports: [FormComponent],
})
export class FormModule {}

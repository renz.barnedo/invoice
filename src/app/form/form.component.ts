import {
  AfterViewChecked,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import {
  AbstractControlOptions,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { exhaustMap, tap } from 'rxjs/operators';
import {
  defaultInvoice,
  Invoice,
  InvoiceStatus,
  Item,
  PaymentTermOptions,
  paymentTermOptions,
} from '../shared/constants/invoice.constants';
import { SharedService } from '../shared/shared.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit, AfterViewChecked, OnDestroy {
  invoiceForm!: FormGroup;
  @ViewChild('formContent', { static: false }) formContentDiv!: ElementRef;
  itemAdded = false;
  isOpened = false;
  formIsOpenSubscription!: Subscription;
  forEditSubscription!: Subscription;
  invoiceForEdit!: Invoice;
  initial!: Invoice;
  toggleCount = -1;
  paymentTerms: PaymentTermOptions = paymentTermOptions;

  constructor(
    private fb: FormBuilder,
    private sharedService: SharedService,
    private cdr: ChangeDetectorRef,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.formIsOpenSubscription = this.sharedService.currentFormIsOpen
      .pipe(
        tap((isOpened: boolean) => {
          this.isOpened = isOpened;
          this.toggleCount++;
        }),
        exhaustMap(() => this.sharedService.currentInvoiceForEdit)
      )
      .subscribe((invoice: Invoice) => {
        this.invoiceForEdit = invoice;
        this.initializeInvoiceForm();
      });
  }

  ngAfterViewChecked(): void {
    if (this.itemAdded) {
      const formContentDiv = <HTMLFormElement>this.formContentDiv.nativeElement;
      formContentDiv.scrollTo(0, formContentDiv.scrollHeight);
      this.itemAdded = false;
    }
    this.cdr.detectChanges();
  }

  ngOnDestroy(): void {
    this.formIsOpenSubscription.unsubscribe();
    this.forEditSubscription.unsubscribe();
  }

  closeForm(): void {
    this.sharedService.toggleForm(false);
  }

  get defaultValidators(): AbstractControlOptions {
    return {
      validators: [Validators.required],
      updateOn: 'blur',
    };
  }

  initializeInvoiceForm(): void {
    let initial = this.invoiceForEdit || defaultInvoice;
    this.initial = initial;
    const { senderAddress, clientAddress, items } = initial;
    this.invoiceForm = this.fb.group({
      ...initial,
      senderAddress: this.fb.group({
        street: this.fb.control(senderAddress.street, Validators.required),
        city: this.fb.control(senderAddress.city, Validators.required),
        postCode: this.fb.control(senderAddress.postCode, Validators.required),
        country: this.fb.control(senderAddress.country, Validators.required),
      }),
      clientName: this.fb.control(initial.clientName, Validators.required),
      clientEmail: this.fb.control(initial.clientEmail, Validators.required),
      clientAddress: this.fb.group({
        street: this.fb.control(clientAddress.street, Validators.required),
        city: this.fb.control(clientAddress.city, Validators.required),
        postCode: this.fb.control(clientAddress.postCode, Validators.required),
        country: this.fb.control(clientAddress.country, Validators.required),
      }),
      createdAt: this.fb.control(initial.createdAt, Validators.required),
      paymentTerms: this.fb.control(initial.paymentTerms, Validators.required),
      description: this.fb.control(initial.description, Validators.required),
      items: this.fb.array(items.map((item: Item) => this.setItemGroup(item))),
    });
  }

  async updateInvoice(invoice: Invoice) {
    const fullInvoice: Invoice = await new Promise(async (resolve, reject) =>
      (invoice.id
        ? await this.sharedService.updateInvoice(invoice)
        : await this.sharedService.addInvoice(invoice)
      ).subscribe(
        (response: Invoice) => resolve(response),
        (error: Error) => reject(error)
      )
    );
    this.closeForm();
    this.invoiceForm.reset();
    this.sharedService.getInvoice(fullInvoice);
    this.router.navigate(['invoice', fullInvoice.id]);
  }

  get invalidControls() {
    const invalid = [];
    const controls = this.invoiceForm.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    return invalid;
  }

  onInvoiceFormSubmit(status: InvoiceStatus): void {
    const invoice: Invoice = {
      ...this.invoiceForm.value,
      status,
      paymentTerms: +this.invoiceForm.value.paymentTerms,
    };
    if (status === 'draft') {
      this.updateInvoice(invoice);
      return;
    }
    if (this.invoiceForm.invalid) {
      return;
    }
    this.updateInvoice(invoice);
  }

  setItemGroup(item: Item) {
    const { name, quantity, price, total } = item;
    return this.fb.group({
      name: this.fb.control(name, Validators.required),
      quantity: this.fb.control(quantity, Validators.required),
      price: this.fb.control(price, [
        Validators.required,
        Validators.min(1),
        Validators.max(999999999),
      ]),
      total,
    });
  }

  get items() {
    return this.invoiceForm.get('items') as FormArray;
  }

  get itemsControls() {
    return this.items.controls;
  }

  getItemGroup(index: number) {
    return <FormGroup>this.itemsControls[index];
  }

  getTotal(index: number) {
    const item = this.getItemGroup(index);
    const total = item.get('total') as FormControl;
    return total.value || 0;
  }

  setItemTotal(index: number): void {
    const item = this.getItemGroup(index);
    const quantity = <FormControl>item.get('quantity');
    const price = item.get('price') as FormControl;
    const total = item.get('total') as FormControl;
    total.setValue(quantity.value * price.value);
  }

  addNewItem(): void {
    const item = { name: '', quantity: 1, price: 1, total: 1 };
    this.itemsControls.push(this.setItemGroup(item));
    this.itemAdded = true;
  }

  deleteItem(index: number): void {
    this.items.removeAt(index);
  }
}
